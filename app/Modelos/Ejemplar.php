<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Ejemplar extends Model
{
    protected $table = 'ejemplares';

    protected $fillable = [
        'numero_ejemplar',
        'estado',
        'localizacion',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    const DISPONIBLE = 1;
    const RESERVADO  = 2;
    const PRESTADO   = 3;

    public function libro()
    {
        return $this->belongsTo(Libro::class);
    }

    public function getEstadoAttribute()
    {
        switch ($this->attributes['estado']) {
            case 1: return 'Disponible'; break;
            case 2: return 'Reservado'; break;
            case 3: return 'Prestado'; break;
        }
    }
}
