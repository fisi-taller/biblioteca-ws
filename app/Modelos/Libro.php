<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Libro extends Model
{
    protected $fillable = [
        'titulo',
        'isbn',
        'edicion',
        'fecha_edicion',
        'cantidad_ejemplares',
        'categoria_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'categoria_id'
    ];

    protected $dates = [
        'fecha_edicion'
    ];

    public function autores()
    {
        return $this->belongsToMany(Autor::class);
    }

    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }

    public function ejemplares()
    {
        return $this->hasMany(Ejemplar::class);
    }

    public function getFechaEdicionAttribute()
    {
        return Carbon::parse($this->attributes['fecha_edicion'])->format('d/m/Y');
    }

    public function scopeBuscar($query, $entrada)
    {
        return $query->where('titulo', 'LIKE', '%'.$entrada.'%')
        ->orWhereHas('autores', function ($autor) use ($entrada) {
            $autor->where('nombre', 'LIKE', '%'.$entrada.'%');
        });
    }
}
