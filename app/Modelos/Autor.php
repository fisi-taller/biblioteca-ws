<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    protected $table = 'autores';

    protected $fillable = ['nombre'];

    protected $hidden = [
        'pivot',
        'created_at',
        'updated_at'
    ];

    public function libros()
    {
        return $this->belongsToMany(Libro::class);
    }
}
