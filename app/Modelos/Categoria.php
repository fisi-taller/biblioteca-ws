<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = ['descripcion'];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function libros()
    {
        return $this->hasMany(Libro::class);
    }
}
