<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Facultad extends Model
{
    protected $table = 'facultades';

    protected $fillable = ['nombre'];

    public function alumnos()
    {
        return $this->hasMany(Usuario::class);
    }
}
