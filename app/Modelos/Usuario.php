<?php

namespace App\Modelos;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Usuario extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const HABILITADO = 1;
    const CASTIGADO  = 2;
    const DE_BAJA    = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'email',
        'codigo',
        'password',
        'telefono',
        'estado',
        'facultad_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at',
    ];

    public function findForPassport($username)
    {
        return $this->where('codigo', $username)->first();
    }

    public function facultad()
    {
        return $this->belongsTo(Facultad::class);
    }

    public function empleado()
    {
        return $this->hasOne(Empleado::class);
    }

    public function getEstadoAttribute()
    {
        switch ($this->attributes['estado']) {
            case 1: return 'Habilitado'; break;
            case 2: return 'Castigado'; break;
            case 3: return 'De baja'; break;
        }
    }

    public function prestamos()
    {
        return $this->hasMany(Prestamo::class);
    }

    public function scopeCastigados($query)
    {
        $query->where('estado', static::CASTIGADO);
    }
}
