<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $fillable = [
        'usuario_id',
        'puesto',
    ];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }
}
