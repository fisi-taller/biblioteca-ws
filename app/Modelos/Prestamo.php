<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Prestamo extends Model
{
    protected $fillable = [
        'empleado_id',
        'usuario_id',
        'ejemplar_id',
        'estado',
        'fecha_prestamo',
        'plazo_maximo',
        'fecha_devolucion'
    ];

    protected $dates = [
        'fecha_prestamo',
        'plazo_maximo',
        'fecha_devolucion',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'fecha_prestamo_formateado',
        'plazo_maximo_formateado',
    ];

    const EN_RESERVA  = 1;
    const EN_PRESTAMO = 2;
    const DEVUELTO    = 3;

    public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }

    public function empleado()
    {
        return $this->belongsTo(Empleado::class);
    }

    public function ejemplar()
    {
        return $this->belongsTo(Ejemplar::class);
    }

    public function getLibroAttribute()
    {
        return $this->ejemplar->libro;
    }

    public function getEstadoAttribute()
    {
        switch ($this->attributes['estado']) {
            case 1: return 'En reserva'; break;
            case 2: return 'En prestamo'; break;
            case 3: return 'Devuelto'; break;
        }
    }

    public function getFechaPrestamoFormateadoAttribute()
    {
        return Carbon::parse($this->attributes['fecha_prestamo'])->format('d/m/Y');
    }

    public function getPlazoMaximoFormateadoAttribute()
    {
        return Carbon::parse($this->attributes['plazo_maximo'])->format('d/m/Y');
    }
}
