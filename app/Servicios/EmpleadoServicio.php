<?php

namespace App\Servicios;

use App\Http\Requests\Api\RegistrarEmpleadoRequest;

use App\Modelos\Empleado;
use App\Modelos\Usuario;

class EmpleadoServicio extends ServicioRest
{
    const FACULTAD_FISICA = 12;

    public function registrar(RegistrarEmpleadoRequest $request)
    {
        $nombre   = $request->input('nombre');
        $codigo   = $request->input('codigo');
        $email    = $request->input('email');
        $puesto   = $request->input('puesto');
        $telefono = $request->input('telefono');

        $usuario = Usuario::create([
            'nombre'      => $nombre,
            'codigo'      => $codigo,
            'email'       => $email,
            'telefono'    => $telefono,
            'password'    => bcrypt(strrev($codigo)),
            'estado'      => Usuario::HABILITADO,
            'facultad_id' => self::FACULTAD_FISICA,
        ]);

        Empleado::create([
            'puesto'     => $puesto,
            'usuario_id' => $usuario->id,
        ]);

        return response(['mensaje' => 'empleado registrado'], 201);
    }
}
