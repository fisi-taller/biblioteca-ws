<?php

namespace App\Servicios;

use App\Http\Requests\Api\RegistrarAlumnoRequest;

use App\Modelos\Usuario;

class AlumnoServicio extends ServicioRest
{
    public function registrar(RegistrarAlumnoRequest $request)
    {
        $nombre   = $request->input('nombre');
        $codigo   = $request->input('codigo');
        $email    = $request->input('email');
        $telefono = $request->input('telefono');
        $facultad = $request->input('facultad');

        Usuario::create([
            'nombre'      => $nombre,
            'codigo'      => $codigo,
            'email'       => $email,
            'estado'      => Usuario::HABILITADO,
            'telefono'    => $telefono,
            'password'    => bcrypt(strrev($codigo)),
            'facultad_id' => $facultad,
        ]);

        return response(['mensaje' => 'alumno registrado'], 201);
    }

    /**
     * Habilitar alumnos que hayan sido castigados anteriormente
     * @return void
     */
    public static function habilitarAlumnos()
    {
        $hoy = Carbon::today();

        Usuario::castigados()
        ->get()
        ->each(function ($usuario) use ($hoy) {
            $ultimo_prestamo = $usuario->prestamos->last();

            if ($hoy->gte($ultimo_prestamo->fecha_devolucion->addDays(3))) {
                $usuario->update(['estado' => Usuario::HABILITADO]);
            }
        });
    }
}
