<?php

namespace App\Servicios;

use App\Modelos\Facultad;

class FacultadServicio extends ServicioRest
{
    public function index()
    {
        $facultades = Facultad::all(['id', 'nombre']);

        return response()->json(compact('facultades'));
    }
}
