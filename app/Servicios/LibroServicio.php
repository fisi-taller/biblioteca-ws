<?php

namespace App\Servicios;

use App\Http\Requests\Api\RegistrarLibroRequest;
use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Modelos\Libro;
use App\Modelos\Ejemplar;
use App\Modelos\Autor;

class LibroServicio extends ServicioRest
{
    public function store(RegistrarLibroRequest $request)
    {
        $titulo              = $request->input('titulo');
        $isbn                = $request->input('isbn');
        $edicion             = $request->input('edicion');
        $fecha_edicion       = Carbon::createFromFormat('d/m/Y', $request->input('fecha_edicion'))->format('Y-m-d');
        $cantidad_ejemplares = $request->input('cantidad_ejemplares');
        $categoria_id        = $request->input('categoria_id');
        $autores             = $request->input('autores');

        $libro = Libro::create([
            'titulo'              => $titulo,
            'isbn'                => $isbn,
            'edicion'             => $edicion,
            'fecha_edicion'       => $fecha_edicion,
            'cantidad_ejemplares' => $cantidad_ejemplares,
            'categoria_id'        => $categoria_id,
        ]);

        for ($i = 1; $i <= $libro->cantidad_ejemplares; $i++) {
            $libro->ejemplares()->save(new Ejemplar([
                'numero_ejemplar' => $libro->isbn . "-$i",
                'estado'          => Ejemplar::DISPONIBLE,
                'localizacion'    => 'almacen',
            ]));
        }

        if (count($autores) == 0) {
            $autor = Autor::firstOrCreate(['nombre' => 'Anónimo']);

            $libro->autores()->attach($autor->id);
        } else {
            foreach ($autores as $nombre_autor) {
                $autor = Autor::firstOrCreate([
                    'nombre' => $nombre_autor
                ]);

                $libro->autores()->attach($autor->id);
            }
        }

        return response()->json(['message' => 'libro registrado'], 201);
    }

    public function search(Request $request)
    {
        $libros = Libro::with('autores', 'categoria')
        ->buscar($request->input('q'))
        ->get();

        if ($libros->count() == 0) {
            return response()->json(['message' => 'no se encontraron coincidencias'], 404);
        }

        return response()->json(compact('libros'));
    }
}
