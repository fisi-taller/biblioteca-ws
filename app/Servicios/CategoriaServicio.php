<?php

namespace App\Servicios;

use App\Modelos\Categoria;

class CategoriaServicio extends ServicioRest
{
    public function index()
    {
        $categorias = Categoria::all(['id', 'descripcion']);

        return response()->json(compact('categorias'));
    }
}
