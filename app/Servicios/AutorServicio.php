<?php

namespace App\Servicios;

use App\Modelos\Autor;

class AutorServicio extends ServicioRest
{
    public function index()
    {
        $autores = Autor::all(['id', 'nombre']);

        return response()->json(compact('autores'));
    }
}
