<?php

namespace App\Servicios;

use App\Http\Requests\Api\ListarPrestamoRequest;

use App\Modelos\Prestamo;

class ReservaServicio extends ServicioRest
{
    public function index(ListarPrestamoRequest $request)
    {
        $reservas = Prestamo::with('usuario', 'ejemplar.libro')
        ->where('estado', Prestamo::EN_RESERVA)
        ->get();;

        return response()->json(compact('reservas'));
    }
}
