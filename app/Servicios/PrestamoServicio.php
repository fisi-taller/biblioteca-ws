<?php

namespace App\Servicios;

use App\Http\Requests\Api\ReservarLibroRequest;
use App\Http\Requests\Api\PrestarLibroRequest;
use App\Http\Requests\Api\DevolverLibroRequest;
use App\Http\Requests\Api\ListarPrestamoRequest;

use App\Modelos\Prestamo;
use App\Modelos\Ejemplar;
use App\Modelos\Usuario;
use Carbon\Carbon;
use DB;

class PrestamoServicio extends ServicioRest
{
    /**
     * Obtener solo prestamos
     * @return Collection
     */
    public function index(ListarPrestamoRequest $request)
    {
        $prestamos = Prestamo::with('usuario', 'ejemplar.libro')
        ->where('estado', Prestamo::EN_PRESTAMO)
        ->get();

        return response()->json(compact('prestamos'));
    }

    /**
     * Verificar si el alumno tiene actualmente algun libro en reserva
     * @param  User $usuario
     * @return boolean
     */
    private function tieneOtraReserva($usuario)
    {
        return $usuario->prestamos->contains(function ($prestamo) {
            return $prestamo->estado == 'En reserva';
        });
    }

    /**
     * Reservar un libro si es que existe un ejemplar disponible de dicho libro
     * @param  integer $usuario id del Usuario habilitado
     * @param  integer $libro   id del Libro
     * @return boolean
     */
    private function intentarReservar($usuario, $libro)
    {
        $respuesta = false;

        try {
            DB::transaction(function () use ($usuario, $libro, &$respuesta) {
                $ejemplares = Ejemplar::where('libro_id', $libro)->get();

                $ejemplar_disponible = $ejemplares->first(function ($ejemplar) {
                    return $ejemplar->estado == 'Disponible';
                });

                if ($ejemplar_disponible) {
                    $ejemplar_disponible->estado = Ejemplar::RESERVADO;
                    $ejemplar_disponible->save();

                    Prestamo::create([
                        'usuario_id'  => $usuario->id,
                        'estado'      => Prestamo::EN_RESERVA,
                        'ejemplar_id' => $ejemplar_disponible->id,
                    ]);

                    $respuesta = true;
                }
            });
        } catch (\Exception $e) {
            return false;
        }

        return $respuesta;
    }

    /**
     * Servicio para reservar un libro si es que existe un ejemplar disponible de dicho libro
     * y si el usuario no tiene otra reserva actualmente
     * @param  integer $usuario id del Usuario habilitado
     * @param  integer $libro   id del Libro
     * @return boolean
     */
    public function reservar(ReservarLibroRequest $request)
    {
        $usuario = $request->user();
        $libro   = $request->input('libro');

        if ($this->tieneOtraReserva($usuario)) {
            return response()->json(['error' => 'el usuario ya tiene otro libro reservado'], 409);
        }

        if ($this->intentarReservar($usuario, $libro)) {
            return response()->json(['mensaje' => 'reserva realizada con exito.'], 201);
        }

        return response()->json(['error' => 'no hay ejemplares disponibles'], 409);
    }

    /**
     * Prestar un libro
     * @param  integer $reserva  id de la reserva
     * @param  integer $empleado  id del empleado
     * @return void
     */
    public function prestar(PrestarLibroRequest $request)
    {
        $reserva  = $request->input('reserva');
        $empleado = $request->user()->empleado;

        $hoy     = Carbon::today();
        $reserva = Prestamo::find($reserva);

        $reserva->update([
            'estado'         => Prestamo::EN_PRESTAMO,
            'empleado_id'    => $empleado->id,
            'fecha_prestamo' => $hoy,
            'plazo_maximo'   => $hoy->addDays(3)
        ]);

        $reserva->ejemplar->update(['estado' => Ejemplar::PRESTADO]);

        return response()->json(['mensaje' => 'prestamo realizado con exito']);
    }

    /**
     * Devolver el libro, se actualiza el estado del ejemplar y del prestamo
     * Se registra ademas la fecha de devolucion para determinar si el usuario sera castigado o no
     * @param  integer $prestamo id valido de prestamo
     * @return void
     */
    public function devolver(DevolverLibroRequest $request)
    {
        $prestamo = $request->input('prestamo');

        $prestamo = Prestamo::find($prestamo);
        $prestamo->update([
            'estado'           => Prestamo::DEVUELTO,
            'fecha_devolucion' => Carbon::today()
        ]);

        $prestamo->ejemplar->update(['estado' => Ejemplar::DISPONIBLE]);

        if ($prestamo->fecha_devolucion->gt($prestamo->plazo_maximo)) {
            $prestamo->usuario->update(['estado' => Usuario::CASTIGADO]);
        }

        return response()->json(['mensaje' => 'ejemplar prestado devuelto con exito']);
    }
}
