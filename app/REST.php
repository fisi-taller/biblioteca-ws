<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('categorias', ['as' => 'api.categoria.todos', 'uses' => 'CategoriaServicio@index']);

Route::get('autores', ['as' => 'api.autores.todos', 'uses' => 'AutorServicio@index']);

Route::get('facultades', ['as' => 'api.facultades.todos', 'uses' => 'FacultadServicio@index']);

Route::get('/libros', ['as' => 'api.libros.busqueda', 'uses' => 'LibroServicio@search']);

Route::middleware('auth:api')->group(function(){
    Route::post('libros', ['as' => 'api.libros.nuevo', 'uses' => 'LibroServicio@store']);

    Route::post('libros/reservar', ['as' => 'api.libros.reservar', 'uses' => 'PrestamoServicio@reservar']);
    Route::post('libros/prestar', ['as' => 'api.libros.prestar', 'uses' => 'PrestamoServicio@prestar']);
    Route::post('libros/devolver', ['as' => 'api.libros.devolver', 'uses' => 'PrestamoServicio@devolver']);

    Route::get('reservas', ['as' => 'api.reservas.todos', 'uses' => 'ReservaServicio@index']);

    Route::get('prestamos', ['as' => 'api.prestamos.todos', 'uses' => 'PrestamoServicio@index']);

    Route::post('alumnos', ['as' => 'api.alumnos.registrar', 'uses' => 'AlumnoServicio@registrar']);

    Route::post('empleados', ['as' => 'api.empleados.registrar', 'uses' => 'EmpleadoServicio@registrar']);
});
