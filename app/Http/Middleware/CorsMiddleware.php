<?php

namespace App\Http\Middleware;

use Closure;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (env('APP_ENV') != 'testing') {
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Headers: Origin, Content-Type");
        }

        return $response;
    }
}
