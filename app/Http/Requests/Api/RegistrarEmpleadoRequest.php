<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class RegistrarEmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($empleado = $this->user()->empleado) {
            return strtolower($empleado->puesto) == 'administrador';
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'puesto' => 'required',
            'email'  => 'email',
            'codigo' => 'required|alpha_num|min:6',
        ];
    }
}
