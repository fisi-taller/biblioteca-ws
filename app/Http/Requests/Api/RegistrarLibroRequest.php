<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class RegistrarLibroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->empleado;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'              => 'required',
            'isbn'                => 'required',
            'edicion'             => 'required',
            'fecha_edicion'       => 'required|date_format:d/m/Y',
            'cantidad_ejemplares' => 'required|integer|min:1',
            'categoria_id'        => 'required|exists:categorias,id',
        ];
    }
}
