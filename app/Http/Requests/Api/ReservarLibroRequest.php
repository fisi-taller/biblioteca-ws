<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

use App\Modelos\Usuario;

class ReservarLibroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->estado == 'Habilitado';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'libro' => 'required|exists:libros,id'
        ];
    }

    public function messages()
    {
        return [
            'libro.required' => 'no existe el libro',
        ];
    }
}
