<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Libro;
use App\Modelos\Categoria;
use App\Modelos\Autor;
use App\Modelos\Ejemplar;
use Carbon\Carbon;

class LibroTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test para crear un libro
     * Asegurar conexion con la BD y enlace con los Modelos de Eloquent
     */
    public function test_crear_libro()
    {
        $categoria = Categoria::create(['descripcion' => 'Poem']);

        $autor = Autor::create(['nombre' => 'Cesar Vallejo']);

        Libro::create([
            'titulo'              => 'Trilce',
            'isbn'                => '9788437609102',
            'edicion'             => 'Planeta',
            'fecha_edicion'       => Carbon::today()->subYears(5)->format('Y-m-d'),
            'cantidad_ejemplares' => 10,
            'categoria_id'        => $categoria->id,
        ]);

        $libro = Libro::find(1);

        $libro->autores()->save($autor);

        $this->assertTrue($libro->titulo                   == 'Trilce');
        $this->assertTrue($libro->categoria->descripcion   == 'Poem');
        $this->assertTrue($libro->autores->first()->nombre == 'Cesar Vallejo');

        $copies = factory(Ejemplar::class, 10)->create();

        $libro->ejemplares()->saveMany($copies);

        $this->assertTrue($libro->ejemplares->count() == 10);
    }
}
