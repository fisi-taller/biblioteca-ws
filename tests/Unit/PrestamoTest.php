<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Usuario;
use App\Modelos\Empleado;
use App\Modelos\Prestamo;
use App\Modelos\Libro;
use App\Modelos\Ejemplar;
use Carbon\Carbon;

class PrestamoTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test para crear un prestamo
     * Asegurar conexion con la BD y enlace con los Modelos de Eloquent
     */
    public function test_crear_prestamo()
    {
        $estudiante = factory(Usuario::class)->create();
        $empleado   = factory(Empleado::class)->create();

        $trilce = factory(Libro::class)->states('trilce')->create();
        $ejemplar_trilce = factory(Ejemplar::class)->create(['libro_id' => $trilce->id]);

        Prestamo::create([
            'empleado_id'      => $empleado->id,
            'usuario_id'       => $estudiante->id,
            'ejemplar_id'      => $ejemplar_trilce->id,
            'estado'           => Prestamo::EN_PRESTAMO,
            'fecha_prestamo'   => Carbon::today()->format('Y-m-d'),
            'plazo_maximo'     => Carbon::today()->addDays(3)->format('Y-m-d'),
            'fecha_devolucion' => null,
        ]);

        $prestamo = Prestamo::find(1);

        $this->assertTrue($prestamo->usuario->name  == $estudiante->name);
        $this->assertTrue($prestamo->empleado->usuario->name == $empleado->usuario->name);
        $this->assertTrue($prestamo->libro->titulo  == 'Trilce');
    }
}
