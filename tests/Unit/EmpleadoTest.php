<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Empleado;
use App\Modelos\Usuario;

class EmpleadoTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test para crear un empleado
     * Asegurar conexion con la BD y enlace con los Modelos de Eloquent
     */
    public function test_crear_empleado()
    {
        $usuario = factory(Usuario::class)->create(['nombre' => 'test empleado']);

        Empleado::create([
            'usuario_id' => $usuario->id,
            'puesto' => 'test puesto',
        ]);

        $empleado = Empleado::find(1);

        $this->assertTrue($empleado->usuario->nombre == 'test empleado');
        $this->assertTrue($empleado->puesto == 'test puesto');
    }
}
