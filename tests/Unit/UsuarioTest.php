<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Usuario;

class UsuarioTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test para crear un usuario
     * Asegurar conexion con la BD y enlace con los Modelos de Eloquent
     */
    public function test_crear_usuario()
    {
        Usuario::create([
            'nombre'      => 'admin',
            'email'       => 'admin@admin.com',
            'password'    => bcrypt('secret'),
            'telefono'    => '123456789',
            'estado'      => Usuario::HABILITADO,
            'facultad_id' => 1,
            'codigo'      => '12200187',
        ]);

        $usuario = Usuario::find(1);

        $this->assertTrue($usuario->email == 'admin@admin.com');
    }
}
