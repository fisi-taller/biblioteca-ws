<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Facultad;

class FacultadTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test para crear una facultad
     * Asegurar conexion con la BD y enlace con los Modelos de Eloquent
     */
    public function test_crear_facultad()
    {
        Facultad::create([
            'nombre' => 'test facultad',
        ]);

        $facultad = Facultad::find(1);

        $this->assertTrue($facultad->nombre == 'test facultad');
    }
}
