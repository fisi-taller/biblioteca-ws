<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Autor;

class AutorTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test para crear un autor
     * Asegurar conexion con la BD y enlace con los Modelos de Eloquent
     */
    public function test_crear_autor()
    {
        Autor::create([
            'nombre' => 'Cesar Vallejo',
        ]);

        $autor = Autor::find(1);

        $this->assertTrue($autor->nombre == 'Cesar Vallejo');
    }
}
