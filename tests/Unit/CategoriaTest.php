<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Categoria;

class CategoriaTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test para crear una categoria
     * Asegurar conexion con la BD y enlace con los Modelos de Eloquent
     */
    public function test_crear_categoria()
    {
        Categoria::create([
            'descripcion' => 'test categoria',
        ]);

        $categoria = Categoria::find(1);

        $this->assertTrue($categoria->descripcion == 'test categoria');
    }
}
