<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Empleado;

class RegistrarEmpleadoTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    /**
     * Test para registrar un empleado en el sistema
     */
    public function test_registrar_alumno()
    {
        $empleado = factory(Empleado::class)->create(['puesto' => 'administrador']);

        $this->actingAs($empleado->usuario, 'api');

        $response = $this->json('POST', '/api/empleados', [
            'nombre' => 'usuario empleado',
            'codigo' => '76726604',
            'puesto' => 'jefe',
        ]);

        $response->assertStatus(201);
    }
}
