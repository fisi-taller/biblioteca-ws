<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Facultad;
use App\Modelos\Empleado;

class RegistrarAlumnoTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    /**
     * Test para registrar un alumno en el sistema
     */
    public function test_registrar_alumno()
    {
        Facultad::create(['nombre' => 'fake facu']);

        $empleado = factory(Empleado::class)->create();

        $this->actingAs($empleado->usuario, 'api');

        $this->json('POST', '/api/alumnos', [
            'nombre'   => 'usuario alumno',
            'codigo'   => '12200187',
            'facultad' => 1,
        ])
        ->assertStatus(201);
    }
}
