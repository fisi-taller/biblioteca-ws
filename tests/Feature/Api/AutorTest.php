<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Autor;

class AutorTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test para probar obtener todos las autores, verificar la conectividad con la api
     */
    public function testExample()
    {
        factory(Autor::class, 10)->create();

        $this->json('GET', '/api/autores')
        ->assertStatus(200)
        ->assertJsonStructure([
            'autores' => [
                '*' => [
                    'id',
                    'nombre'
                ]
            ]
        ]);
    }
}
