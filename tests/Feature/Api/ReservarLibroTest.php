<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Libro;
use App\Modelos\Usuario;
use App\Modelos\Ejemplar;
use App\Modelos\Prestamo;

class ReservarLibroTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test para realizar una reserva de un libro
     */
    public function test_reservar_libro()
    {
        $trilce   = factory(Libro::class)->states('trilce')->create();
        $ejemplar = factory(Ejemplar::class)->create();
        $usuario  = factory(Usuario::class)->create();

        $trilce->ejemplares()->save($ejemplar);

        $this->actingAs($usuario, 'api');

        $response = $this->json('POST', '/api/libros/reservar', [
            'libro' => $trilce->id,
        ]);

        $response->assertStatus(201);

        $prestamo = Prestamo::find(1);

        $this->assertDatabaseHas('prestamos', [
            'usuario_id' => $usuario->id,
            'estado'     => Prestamo::EN_RESERVA
        ]);
    }

    /**
     * Test para validar que solo se reserven libros con ejemplares disponibles
     */
    public function test_no_puede_reservar_si_no_hay_ejemplar_disponible()
    {
        $trilce   = factory(Libro::class)->states('trilce')->create();
        $ejemplar = factory(Ejemplar::class)->create(['estado' => Ejemplar::RESERVADO]);
        $usuario  = factory(Usuario::class)->create();

        $trilce->ejemplares()->save($ejemplar);

        $this->actingAs($usuario, 'api');

        $response = $this->json('POST', '/api/libros/reservar', [
            'libro' => $trilce->id,
        ]);

        $response->assertStatus(409);
    }

    /**
     * Test para validar que el libro existe
     */
    public function test_no_puede_reservar_si_libro_no_existe()
    {
        $trilce  = factory(Libro::class)->states('trilce')->make();
        $usuario = factory(Usuario::class)->create();

        $this->actingAs($usuario, 'api');

        $response = $this->json('POST', '/api/libros/reservar', [
            'libro' => $trilce->id,
        ]);

        $response->assertStatus(422)
        ->assertJson(['libro' => ['no existe el libro']]);
    }

    /**
     * Test para verificar que un alumno no pueda realizar dos reservas si aun tiene otra reserva
     */
    public function test_no_puede_reservar_dos_libros()
    {
        $trilce     = factory(Libro::class)->states('trilce')->create();
        $ejemplares = factory(Ejemplar::class, 2)->create();
        $usuario    = factory(Usuario::class)->create();

        $trilce->ejemplares()->saveMany($ejemplares);

        $this->actingAs($usuario, 'api');

        $response = $this->json('POST', '/api/libros/reservar', [
            'libro' => $trilce->id,
        ]);

        $response->assertStatus(201);

        $usuario = $usuario->fresh();

        $this->actingAs($usuario, 'api');

        $response = $this->json('POST', '/api/libros/reservar', [
            'libro' => $trilce->id,
        ]);

        $response->assertStatus(409)
        ->assertJson(['error' => 'el usuario ya tiene otro libro reservado']);
    }
}
