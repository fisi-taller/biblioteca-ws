<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Categoria;

class CategoriaTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * Test para probar obtener todas las categorias, verificar la conectividad con la api
     */
    public function test_api_todas_las_categorias()
    {
        factory(Categoria::class, 10)->create();

        $response = $this->json('GET', '/api/categorias');

        $response->assertStatus(200)
        ->assertJsonStructure([
            'categorias' => [
                '*' => [
                    'id',
                    'descripcion'
                ]
            ]
        ]);
    }
}
