<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Empleado;
use App\Modelos\Libro;
use App\Modelos\Ejemplar;
use App\Modelos\Usuario;
use App\Modelos\Prestamo;
use Carbon\Carbon;

class DevolverLibroTest extends TestCase
{
     use DatabaseMigrations;

    /**
     * Test para realizar una devolucion de un libro
     */
    public function test_devolver_libro()
    {
        $trilce   = factory(Libro::class)->states('trilce')->create();
        $ejemplar = factory(Ejemplar::class)->create(['estado' => Ejemplar::PRESTADO]);
        $usuario  = factory(Usuario::class)->create();
        $empleado = factory(Empleado::class)->create();

        $trilce->ejemplares()->save($ejemplar);

        $yesterday = Carbon::yesterday();

        $prestamo = Prestamo::create([
            'empleado_id'    => $empleado->id,
            'usuario_id'     => $usuario->id,
            'ejemplar_id'    => $ejemplar->id,
            'estado'         => Prestamo::EN_PRESTAMO,
            'fecha_prestamo' => $yesterday->format('Y-m-d'),
            'plazo_maximo'   => $yesterday->addDays(3)->format('Y-m-d'),
        ]);

        $this->actingAs($empleado->usuario, 'api');

        $response = $this->json('POST', '/api/libros/devolver', [
            'prestamo' => $prestamo->id,
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('prestamos', [
            'usuario_id' => $usuario->id,
            'estado'     => Prestamo::DEVUELTO,
        ]);

        $usuario = Usuario::find(1);

        $this->assertTrue($usuario->estado == 'Habilitado');
    }

    /**
     * Test para realizar una devolucion de un libro el mismo dia que el plazo maximo
     */
    public function test_devolver_libro_mismo_dia_plazo_maximo()
    {
        $trilce   = factory(Libro::class)->states('trilce')->create();
        $ejemplar = factory(Ejemplar::class)->create(['estado' => Ejemplar::PRESTADO]);
        $usuario  = factory(Usuario::class)->create();
        $empleado = factory(Empleado::class)->create();

        $trilce->ejemplares()->save($ejemplar);

        $yesterday = Carbon::yesterday();

        $prestamo = Prestamo::create([
            'empleado_id'    => $empleado->id,
            'usuario_id'     => $usuario->id,
            'ejemplar_id'    => $ejemplar->id,
            'estado'         => Prestamo::EN_PRESTAMO,
            'fecha_prestamo' => $yesterday->format('Y-m-d'),
            'plazo_maximo'   => $yesterday->addDays(3)->format('Y-m-d'),
        ]);

        // Despues de tres dias a las 8 am
        Carbon::setTestNow($yesterday->addHours(8));

        $this->actingAs($empleado->usuario, 'api');

        $response = $this->json('POST', '/api/libros/devolver', [
            'prestamo' => $prestamo->id,
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('prestamos', [
            'usuario_id' => $usuario->id,
            'estado'     => Prestamo::DEVUELTO,
        ]);

        $this->assertDatabaseHas('ejemplares', [
            'estado' => Ejemplar::DISPONIBLE,
        ]);

        $this->assertDatabaseHas('usuarios', [
            'estado' => Usuario::HABILITADO,
        ]);

        $usuario = Usuario::find(1);

        $this->assertTrue($usuario->estado == 'Habilitado');
    }

    /**
     * Test para realizar una devolucion de un libro despues del plazo maximo
     */
    public function test_devolver_libro_despues_plazo_maximo()
    {
        $trilce   = factory(Libro::class)->states('trilce')->create();
        $ejemplar = factory(Ejemplar::class)->create(['estado' => Ejemplar::PRESTADO]);
        $usuario  = factory(Usuario::class)->create();
        $empleado = factory(Empleado::class)->create();

        $trilce->ejemplares()->save($ejemplar);

        $yesterday = Carbon::yesterday();

        $prestamo = Prestamo::create([
            'empleado_id'    => $empleado->id,
            'usuario_id'     => $usuario->id,
            'ejemplar_id'    => $ejemplar->id,
            'estado'         => Prestamo::EN_PRESTAMO,
            'fecha_prestamo' => $yesterday->format('Y-m-d'),
            'plazo_maximo'   => $yesterday->addDays(3)->format('Y-m-d'),
        ]);

        // Despues de tres dias a las 8 am
        Carbon::setTestNow($yesterday->addDay());

        $this->actingAs($empleado->usuario, 'api');

        $response = $this->json('POST', '/api/libros/devolver', [
            'prestamo' => $prestamo->id,
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('prestamos', [
            'usuario_id' => $usuario->id,
            'estado'     => Prestamo::DEVUELTO,
        ]);

        $this->assertDatabaseHas('ejemplares', [
            'estado' => Ejemplar::DISPONIBLE,
        ]);

        $usuario = Usuario::find(1);

        $this->assertTrue($usuario->estado == 'Castigado');
    }
}
