<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Modelos\Categoria;
use App\Modelos\Ejemplar;
use App\Modelos\Empleado;

class RegistrarLibroTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    /**
     * Test para probar que se esta registrando un libro exitosamente mediante la API
     * Verificar su existencia en la BD
     * Por defecto la localizacion de los ejemplares es en el almacen
     */
    public function test_registrar_libro()
    {
        factory(Categoria::class)->create();

        $empleado = factory(Empleado::class)->create();

        $this->actingAs($empleado->usuario, 'api');

        $response = $this->json('POST', '/api/libros', [
            'titulo'              => 'Test api libro',
            'isbn'                => '1234567890123',
            'edicion'             => 'Test edicion',
            'fecha_edicion'       => '14/04/2000',
            'cantidad_ejemplares' => 10,
            'categoria_id'        => 1,
            'autores'             => [
                'Cesar Vallejo'
            ]
        ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas('libros', [
            'titulo' => 'Test api libro',
        ]);

        $cantidad_ejemplares = Ejemplar::where('libro_id', 1)->get();

        $this->assertTrue($cantidad_ejemplares->count() == 10);

        $this->assertDatabaseHas('autores', [
            'nombre' => 'Cesar Vallejo',
        ]);
    }

    public function test_no_puede_registrar_libro_con_cantidad_negativa_o_cero()
    {
        factory(Categoria::class)->create();

        $empleado = factory(Empleado::class)->create();

        $this->actingAs($empleado->usuario, 'api');

        $this->json('POST', '/api/libros', [
            'titulo'              => 'Test api libro',
            'isbn'                => '1234567890123',
            'edicion'             => 'Test edicion',
            'fecha_edicion'       => '14/04/2000',
            'cantidad_ejemplares' => -10,
            'categoria_id'        => 1,
        ])
        ->assertStatus(422);

        $this->json('POST', '/api/libros', [
            'titulo'              => 'Test api libro',
            'isbn'                => '1234567890123',
            'edicion'             => 'Test edicion',
            'fecha_edicion'       => '14/04/2000',
            'cantidad_ejemplares' => -1,
            'categoria_id'        => 1,
        ])
        ->assertStatus(422);

        $this->json('POST', '/api/libros', [
            'titulo'              => 'Test api libro',
            'isbn'                => '1234567890123',
            'edicion'             => 'Test edicion',
            'fecha_edicion'       => '14/04/2000',
            'cantidad_ejemplares' => 0,
            'categoria_id'        => 1,
        ])
        ->assertStatus(422);
    }

    public function test_puede_registrar_varios_autores_para_un_libro()
    {
        factory(Categoria::class)->create();

        $empleado = factory(Empleado::class)->create();

        $this->actingAs($empleado->usuario, 'api');

        $response = $this->json('POST', '/api/libros', [
            'titulo'              => 'Design Patterns',
            'isbn'                => '1234567890123',
            'edicion'             => 'Test edicion',
            'fecha_edicion'       => '14/04/2000',
            'cantidad_ejemplares' => 10,
            'categoria_id'        => 1,
            'autores'             => [
                'Erich Gamma',
                'Richard Helm',
                'Ralph Johnson',
                'John Vlissides'
            ]
        ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas('autores', ['nombre' => 'Erich Gamma']);
        $this->assertDatabaseHas('autores', ['nombre' => 'Richard Helm']);
        $this->assertDatabaseHas('autores', ['nombre' => 'Ralph Johnson']);
        $this->assertDatabaseHas('autores', ['nombre' => 'John Vlissides']);
    }
}
