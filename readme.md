# Sistema de Biblioteca

## Entorno necesario

- LAMP o Homestead (Vagrant)
- Composer
- PHP >= 5.6.7

[Lamp](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04)

[Homestead](https://laravel.com/docs/5.4/homestead)

```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '55d6ead61b29c7bdee5cccfb50076874187bd9f21f65d8991d46ec5cc90518f447387fb9f76ebae1fbbacf329e583e30') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

```
mv composer.phar /usr/local/bin/composer
```

## Instalacion

- Instalando el framework y las dependencias

```
composer install
```

- Estableciendo variables de entorno (Copiamos el .env.example)

```
cp .env.example .env
```

- Lo editamos con los valores de nuestro entorno

- Creamos el app key para la aplicacion

```
php artisan key:generate
art key:generate           (Solo Homestead)
```

- Corremos las migraciones y alimentamos la bd con los seeds

```
php artisan migrate --seed
art migrate --seed         (Solo Homestead)
```

- Creamos los keys de encriptacion para generar tokens seguros. Ademas, vamos a crear clientes oauth con tokens con permisos de "personal access" y "password grant"

```
php artisan passport:install
art passport:install       (Solo Homestead)
```

- Probar cliente oauth

```
/oauth/token
```

### Parametros

- client_id: 2
- client_secret: LoQueHayaSalidoEnElPasoAnterior
- grant_type: password
- username: admin@admin.com
- password: secret

### Respuesta

Lo que importa es tener el access_token

- Copiarlo y usarlo en la siguiente ruta

```
/api/user
```

### Headers (Si usa Postman u otra herramienta parecida)

- Accept: application/json
- Authorization: Bearer {access_token}

