<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Modelos\Usuario::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'nombre'         => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'codigo'         => $faker->randomNumber(9),
        'telefono'       => $faker->phoneNumber,
        'estado'         => App\Modelos\Usuario::HABILITADO,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'facultad_id'    => 1,
    ];
});

$factory->define(App\Modelos\Categoria::class, function (Faker\Generator $faker) {
    return [
        'descripcion' => $faker->text,
    ];
});

$factory->define(App\Modelos\Autor::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name,
    ];
});

$factory->define(App\Modelos\Ejemplar::class, function (Faker\Generator $faker) {
    return [
        'numero_ejemplar' => $faker->randomNumber,
        'estado'          => App\Modelos\Ejemplar::DISPONIBLE,
        'localizacion'    => $faker->text,
        'libro_id'        => 1,
    ];
});

$factory->define(App\Modelos\Libro::class, function (Faker\Generator $faker) {
    return [
        'titulo'              => $faker->userName,
        'isbn'                => $faker->isbn13,
        'edicion'             => $faker->name,
        'fecha_edicion'       => Carbon\Carbon::today()->subYears(5)->format('Y-m-d'),
        'cantidad_ejemplares' => 1,
        'categoria_id'        => 1,
    ];
});

$factory->state(App\Modelos\Libro::class, 'trilce', function (Faker\Generator $faker) {
    return [
        'titulo'  => 'Trilce',
        'isbn'    => '9788437609102',
        'edicion' => 'Planeta',
    ];
});

$factory->define(App\Modelos\Empleado::class, function (Faker\Generator $faker) use ($factory) {
    return [
        'usuario_id' => factory(App\Modelos\Usuario::class)->create()->id,
        'puesto'     => $faker->name,
    ];
});
