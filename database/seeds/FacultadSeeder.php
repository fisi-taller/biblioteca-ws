<?php

use Illuminate\Database\Seeder;

class FacultadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facultades')->delete();
        DB::table('facultades')->insert([
            ['nombre' => 'Facultad de Medicina'],
            ['nombre' => 'Facultad de Farmacia y Bioquímica'],
            ['nombre' => 'Facultad de Odontología'],
            ['nombre' => 'Facultad de Medicina Veterinaria'],
            ['nombre' => 'Facultad de Psicología'],
            ['nombre' => 'Facultad de Letras y Ciencias Humanas'],
            ['nombre' => 'Facultad de Educación'],
            ['nombre' => 'Facultad de Derecho y Ciencia Política'],
            ['nombre' => 'Facultad de Ciencias Sociales'],
            ['nombre' => 'Facultad de Química e Ingeniería Química'],
            ['nombre' => 'Facultad de Ciencias Biológicas'],
            ['nombre' => 'Facultad de Ciencias Físicas'],
            ['nombre' => 'Facultad de Ciencias Matemáticas'],
            ['nombre' => 'Facultad de Ingeniería Geológica, Minera, Metalúrgica y Geográfica'],
            ['nombre' => 'Facultad de Ingeniería Industrial'],
            ['nombre' => 'Facultad de Ingeniería Electrónica y Eléctrica'],
            ['nombre' => 'Facultad de Ingeniería de Sistemas e Informática'],
            ['nombre' => 'Facultad de Ciencias Administrativas'],
            ['nombre' => 'Facultad de Ciencias Contables'],
            ['nombre' => 'Facultad de Ciencias Económicas'],
        ]);
    }
}
