<?php

use Illuminate\Database\Seeder;

use App\Modelos\Categoria;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->truncate();

        Categoria::create(['descripcion' => 'Administracion']);
        Categoria::create(['descripcion' => 'Algebra']);
        Categoria::create(['descripcion' => 'Arte']);
        Categoria::create(['descripcion' => 'Autoayuda']);
        Categoria::create(['descripcion' => 'Biografia']);
        Categoria::create(['descripcion' => 'Biologia']);
        Categoria::create(['descripcion' => 'Clasico']);
        Categoria::create(['descripcion' => 'Critica y teoria literaria']);
        Categoria::create(['descripcion' => 'Ciencia ficcion']);
        Categoria::create(['descripcion' => 'Ciencias exactas']);
        Categoria::create(['descripcion' => 'Ciencias naturales']);
        Categoria::create(['descripcion' => 'Ciencias sociales']);
        Categoria::create(['descripcion' => 'Deportes y juegos']);
        Categoria::create(['descripcion' => 'Divulgacion cientifica']);
        Categoria::create(['descripcion' => 'Didactico']);
        Categoria::create(['descripcion' => 'Drama']);
        Categoria::create(['descripcion' => 'Ensayo']);
        Categoria::create(['descripcion' => 'Estadistica']);
        Categoria::create(['descripcion' => 'Economia']);
        Categoria::create(['descripcion' => 'Finanzas']);
        Categoria::create(['descripcion' => 'Filosofia']);
        Categoria::create(['descripcion' => 'Fisica']);
        Categoria::create(['descripcion' => 'Historia']);
        Categoria::create(['descripcion' => 'Idiomas']);
        Categoria::create(['descripcion' => 'Ingenieria']);
        Categoria::create(['descripcion' => 'Medicina']);
        Categoria::create(['descripcion' => 'Matematica']);
        Categoria::create(['descripcion' => 'Manuales y cursos']);
        Categoria::create(['descripcion' => 'Novela']);
        Categoria::create(['descripcion' => 'Psicologia']);
        Categoria::create(['descripcion' => 'Pedagogia']);
        Categoria::create(['descripcion' => 'Programacion']);
        Categoria::create(['descripcion' => 'Quimica']);
        Categoria::create(['descripcion' => 'Solucionarios']);
        Categoria::create(['descripcion' => 'Tesis']);
        Categoria::create(['descripcion' => 'Tecnologia']);
        Categoria::create(['descripcion' => 'Varios generos']);
    }
}
