<?php

use Illuminate\Database\Seeder;

use App\Modelos\Usuario;
use App\Modelos\Empleado;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorySeeder::class);
        $this->call(AutorSeeder::class);
        $this->call(FacultadSeeder::class);

        Usuario::create(['nombre' => 'admin', 'email' => 'admin@admin.com', 'password' => bcrypt('secret'), 'telefono' => '123456789', 'estado' => 1, 'facultad_id' => 1, 'codigo' => '123456789']);
        Empleado::create(['usuario_id' => 1, 'puesto' => 'administrador']);
    }
}
